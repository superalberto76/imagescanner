package com.albertoborsetta.imagescanner.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.albertoborsetta.imagescanner.engine.exceptions.ImageScannerException;
import com.albertoborsetta.imagescanner.commons.ImageFileUtils;
import com.albertoborsetta.imagescanner.engine.ImageTemplate;

public class ImageScanner {
	
	public static void main(String[] args) {
		Locale locale = Locale.getDefault();
		ImageFileUtils fileUtils = ImageFileUtils.getInstance(locale);

		String[] extensions = ImageIO.getReaderFileSuffixes();
		Iterator<?> fileIterator = FileUtils.iterateFiles(new File(args[0]), extensions, false);
		HashMap<String, ImageTemplate> filledForms = new HashMap<>();
		long start = System.currentTimeMillis();
		while (fileIterator.hasNext()) {
			File imageFile = (File) fileIterator.next();
			BufferedImage image = null;
			try {
				image = ImageIO.read(imageFile);
			} catch (IOException e) {
				System.out.println(e);
				System.exit(-1);
			}
			ImageTemplate scannedImage = new ImageTemplate(imageFile.getName());
			try {
				System.out.println("Scanning image: " + imageFile.getName());
				scannedImage.findBlackPixels(image);
			} catch (ImageScannerException e) {
				System.out.println(e);
				System.exit(-1);
			}
			filledForms.put(FilenameUtils.getName(imageFile.toString()), scannedImage);
		}
		long end = System.currentTimeMillis();
		System.out.println("Total running time: " + (end - start) + " millis");

		Date today = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		File outputFile = new File(
				args[0] + System.getProperty("file.separator") + "results_" + sdf.format(today) + ".csv");
		System.out.println("Writing output file: " + outputFile.getName());
		fileUtils.saveCsvAs(outputFile, filledForms);
		System.exit(0);
	}

}
