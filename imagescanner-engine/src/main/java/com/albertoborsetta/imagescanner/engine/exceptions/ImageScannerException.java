package com.albertoborsetta.imagescanner.engine.exceptions;

import com.albertoborsetta.formscanner.api.exceptions.FormScannerException;

public class ImageScannerException extends FormScannerException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ImageScannerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ImageScannerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ImageScannerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ImageScannerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ImageScannerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
