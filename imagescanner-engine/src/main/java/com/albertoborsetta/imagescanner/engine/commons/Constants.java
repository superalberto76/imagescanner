package com.albertoborsetta.imagescanner.engine.commons;

public class Constants {

	public static final String BLACK = "BLACK";
	
	public static final String WHITE = "WHITE";

	public static final String TOTAL = "TOTAL";

	public static final String BLACK_RATIO = "BLACK_RATIO";

	public static final String WHITE_RATIO = "WHITE_RATIO";
	
}
