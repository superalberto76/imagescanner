package com.albertoborsetta.imagescanner.engine;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.albertoborsetta.formscanner.api.FormTemplate;
import com.albertoborsetta.imagescanner.engine.commons.Constants;
import com.albertoborsetta.imagescanner.engine.exceptions.ImageScannerException;

public class ImageTemplate extends FormTemplate {

	private static final int THRESHOLD = 127;
	private static final int DENSITY = 0;

	private int blackPixels;
	private int whitePixels;
	private int totalPixels;
	private float blackRatio;
	private float whiteRatio;

	public ImageTemplate(String name) {
		super(name);
	}

	public void findBlackPixels(BufferedImage image) throws ImageScannerException {
		int cores = Runtime.getRuntime().availableProcessors();
		// Only for debug
		// cores = 1;

		ExecutorService threadPool = Executors.newFixedThreadPool(--cores <= 0 ? 1 : cores);
		HashSet<Future<HashMap<String, Integer>>> pixelDetectorThreads = new HashSet<>();

		int subImageHeight = image.getHeight() / cores;
		int subImageWidth = image.getWidth() / cores;

		for (int i = 0; i < cores; i++) {
			for (int j = 0; j < cores; j++) {
				BufferedImage subImage = image.getSubimage(subImageWidth * i, subImageHeight * j, subImageWidth,
						subImageHeight);
				Future<HashMap<String, Integer>> future = threadPool
						.submit(new PixelDetector(THRESHOLD, DENSITY, subImage));
				pixelDetectorThreads.add(future);
			}
		}
		
		for (Future<HashMap<String, Integer>> thread : pixelDetectorThreads) {
			try {
				HashMap<String, Integer> threadFields = thread.get();
				for (String pixelType : threadFields.keySet()) {
					Integer numberOfPixels = threadFields.get(pixelType);
					addPixels(pixelType, numberOfPixels);
				}
			} catch (InterruptedException | ExecutionException e) {
				throw new ImageScannerException(e.getCause());
			}
		}
		
		threadPool.shutdown();
	}

	private void addPixels(String pixelType, Integer numberOfPixels) {
		switch (pixelType) {
		case Constants.BLACK:
			blackPixels = blackPixels + numberOfPixels;
			break;
		case Constants.WHITE:
			whitePixels = whitePixels + numberOfPixels;
			break;
		}
	}

	public int getBlackPixels() {
		return blackPixels;
	}

	public int getWhitePixels() {
		return whitePixels;
	}

	public int getTotalPixels() {
		return blackPixels + whitePixels;
	}

	public double getBlackRatio() {
		return (((double) blackPixels / (blackPixels + whitePixels)) * 100);
	}

	public double getWhiteRatio() {
		return (((double) whitePixels / (blackPixels + whitePixels)) * 100);
	}
}
