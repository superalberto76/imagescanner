package com.albertoborsetta.imagescanner.engine;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.concurrent.Callable;

import com.albertoborsetta.formscanner.api.FormScannerDetector;
import com.albertoborsetta.imagescanner.engine.commons.Constants;

public class PixelDetector extends FormScannerDetector
implements Callable<HashMap<String, Integer>> {

	protected PixelDetector(int threshold, int density, BufferedImage image) {
		super(threshold, density, image);
	}

	@Override
	public HashMap<String, Integer> call() throws Exception {
		int[] rgbArray = new int[1];
		int blackPixels = 0;
		int whitePixels = 0;		
		
		for (int x=0; x<image.getWidth(); x++) {
			for (int y=0; y<image.getHeight(); y++) {
				image.getRGB(x, y, 1, 1, rgbArray, 0, 1);
				if (isWhite(x, y, rgbArray) == WHITE_PIXEL) {
					whitePixels++;
				} else {
					blackPixels++;
				}
			}
		}
		
		HashMap<String, Integer> pixels = new HashMap<>();
		pixels.put(Constants.WHITE, whitePixels);
		pixels.put(Constants.BLACK, blackPixels);
		
		return pixels;
	}
	
	protected int isWhite(int xi, int yi, int[] rgbArray) {
		if ((rgbArray[0] & (0xFF)) < threshold) {
            return BLACK_PIXEL;
        }
        return WHITE_PIXEL;
    }

}
