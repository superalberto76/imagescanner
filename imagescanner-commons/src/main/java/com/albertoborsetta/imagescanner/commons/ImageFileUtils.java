package com.albertoborsetta.imagescanner.commons;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.albertoborsetta.imagescanner.engine.ImageTemplate;
import com.albertoborsetta.imagescanner.engine.commons.Constants;
import com.albertoborsetta.formscanner.commons.FormFileUtils;

public class ImageFileUtils extends FormFileUtils {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public class ImageHeader extends Header {

		public ImageHeader() {
			super();
		}

		public String getFirstHeader() {
			return StringUtils.EMPTY;
		}
	}

	private static ImageFileUtils instance;

	public static ImageFileUtils getInstance(Locale locale) {
		if (instance == null) {
			instance = new ImageFileUtils(locale);
		}
		return instance;
	}

	private ImageFileUtils(Locale locale) {
		super(locale);
	}

	public File saveCsvAs(File file, HashMap<String, ImageTemplate> scannedImages) {
		ImageHeader header = getHeader();
		String[] headerKeys = header.getHeaderKeys(false);
		ArrayList<HashMap<String, String>> results = getResults(scannedImages, header);

		ICsvMapWriter mapWriter = null;
		try {
			try {
				FileOutputStream fos = new FileOutputStream(file);
				OutputStreamWriter out = new OutputStreamWriter(fos, Charset.forName("UTF-8"));
				mapWriter = new CsvMapWriter(out, CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE);
				mapWriter.writeHeader(headerKeys);

				for (HashMap<String, String> result : results) {
					mapWriter.write(result, headerKeys);
				}
			} finally {
				if (mapWriter != null) {
					mapWriter.close();
				}
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		return file;
	}

	protected static ArrayList<HashMap<String, String>> getResults(HashMap<String, ImageTemplate> scannedImages,
			ImageHeader header) {
		ArrayList<HashMap<String, String>> results = new ArrayList<>();

		for (Entry<String, ImageTemplate> scannedImage : scannedImages.entrySet()) {
			ImageTemplate image = scannedImage.getValue();

			HashMap<String, String> result = new HashMap<>();

			result.put(header.getFirstHeader(), scannedImage.getKey());

			for (String headerKey : header.getKeys()) {
				switch (headerKey) {
				case Constants.BLACK:
					result.put(headerKey, ((Integer) image.getBlackPixels()).toString());
					break;
				case Constants.WHITE:
					result.put(headerKey, ((Integer) image.getWhitePixels()).toString());
					break;
				case Constants.TOTAL:
					result.put(headerKey, ((Integer) image.getTotalPixels()).toString());
					break;
				case Constants.BLACK_RATIO:
					result.put(headerKey, ((Double) image.getBlackRatio()).toString());
					break;
				case Constants.WHITE_RATIO:
					result.put(headerKey, ((Double) image.getWhiteRatio()).toString());
					break;
				}
			}
			results.add(result);
		}
		return results;
	}

	public ImageHeader getHeader() {
		ImageHeader header = new ImageHeader();

		header.addHeaderKey(Constants.BLACK);
		header.addHeaderKey(Constants.WHITE);
		header.addHeaderKey(Constants.TOTAL);
		header.addHeaderKey(Constants.BLACK_RATIO);
		header.addHeaderKey(Constants.WHITE_RATIO);

		return header;
	}

}
